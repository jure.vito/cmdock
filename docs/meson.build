docdir = get_option('docdir')
if docdir == ''
  docdir = './docs'
endif

doc_sources = files()
apidoc_sources = files()
# Common files
subdir('_static')

if get_option('doc')
  sphinx = find_program('sphinx-build-3', 'sphinx-build')

  doc_sources += files('conf.py', 'index.rst')
  subdir('developer-guide')
  subdir('getting-started-guide')
  subdir('reference-guide')
  subdir('user-guide')
  subdir('_images')

  version = meson.project_version()
  custom_target('doc',
    output : 'html',
    depend_files : doc_sources,
    command : [sphinx,
               '-D', f'version=@version@', '-D', f'release=@version@',
               meson.current_source_dir(), '@OUTPUT@'],
    console : true,
    install : true,
    install_dir : docdir
  )
endif

if get_option('apidoc')
  doxygen = find_program('doxygen')

  doxyfile = files('Doxyfile')
  apidoc_sources += files('doxy-custom.css', 'index.dox')

  custom_target('apidoc',
    output : 'api',
    input : doxyfile,
    depend_files : [apidoc_sources, srcRbt],
    command : [doxygen, '@INPUT@'],
    console : true,
    install : true,
    install_dir : docdir / 'html',
    env: {
      'PROJECT_NUMBER': meson.project_version(),
      'CURRENT_SOURCE_DIR': meson.current_source_dir(),
      'PROJECT_SOURCE_ROOT': meson.project_source_root(),
      'OUTPUT_DIRECTORY': meson.current_build_dir()
    }
  )
endif
