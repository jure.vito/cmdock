.. Cmdock-documentation:

CmDock documentation
====================================================================================================

A thorough documentation refresh is in preparation.

.. _download:

Download
^^^^^^^^

Please visit the CmDock GitLab page for the most up to date releases.

* `Download a released version <https://gitlab.com/Jukic/cmdock/-/releases>`__
* `Get the latest code using git <https://gitlab.com/Jukic/cmdock>`__

.. _getting-started-guide:

Getting started guide
---------------------

In this section, you have the documentation with installation and validation
instructions for first-time users.

To continue with a short validation experiment (contained in the :ref:`Getting
started guide <getting-started-guide>`), please visit the :ref:`Validation
experiments section <validation-experiments>`.

.. toctree::
   :maxdepth: 2
   :caption: Getting started:

   getting-started-guide/overview
   getting-started-guide/quick-and-dirty-installation
   getting-started-guide/prerequisites
   getting-started-guide/unpacking
   getting-started-guide/building
   getting-started-guide/validation-experiments

.. _reference-guide:

Reference guide
---------------

In this section you can find the documentation containing full explanation of
all |Dock| software package and features.

For installation details and first-users instructions, please visit
:ref:`Installation <quick-and-dirty-installation>` and :ref:`Getting started
<getting-started-guide>` sections.

.. toctree::
   :maxdepth: 2
   :caption: Reference guide:

   reference-guide/preface
   reference-guide/acknowledgements
   reference-guide/introduction
   reference-guide/configuration
   reference-guide/cavity-mapping
   reference-guide/scoring-functions
   reference-guide/docking-protocol
   reference-guide/system-definition-file
   reference-guide/atom-typing
   reference-guide/file-formats
   reference-guide/programs
   reference-guide/appendix

.. _user-guide:

User guide
----------

.. toctree::
   :maxdepth: 2
   :caption: User guide:

   user-guide/docking-in-3-steps
   user-guide/docking-strategies
   user-guide/multi-step-protocol-for-htvs
   user-guide/calculating-roc-curves
   user-guide/running-docking-jobs-in-parallel
   user-guide/pharmacophoric-restraints

.. _developer-guide:

Developer guide
---------------

.. toctree::
   :maxdepth: 2
   :caption: Developer guide:

   developer-guide/target-platforms
   developer-guide/build-system
   developer-guide/coding-standards
   developer-guide/documentation
   developer-guide/versioning

.. _support:

Support
-------


Issue tracker
^^^^^^^^^^^^^

Mostly for developers and code-related problems. If you find a bug, please
report it in `issues section in CmDock GitLab project
<https://gitlab.com/Jukic/cmdock/issues>`__.

References
^^^^^^^^^^

If you are using |Dock| in your research, please cite [CmDock]_. Former
software references provided for completeness are [rDock2014]_ and [RiboDock2004]_.

.. [CmDock] Jukič, M., Škrlj, B., Tomšič, G., Pleško, S., Podlipnik, Č.,
          Bren, U. (2021) Prioritisation of Compounds for 3CLpro Inhibitor
          Development on SARS-CoV-2 Variants. Molecules 26(10): 3003.
          `doi:10.3390/molecules26103003 <https://doi.org/10.3390/molecules26103003>`__

.. [rDock2014] Ruiz-Carmona, S., Alvarez-Garcia, D., Foloppe, N.,
	       Garmendia-Doval, A. B., Juhos S., et al. (2014) rDock: A Fast,
	       Versatile and Open Source Program for Docking Ligands to Proteins
	       and Nucleic Acids. PLoS Comput Biol 10(4): e1003571.
	       `doi:10.1371/journal.pcbi.1003571 <https://doi.org/10.1371/journal.pcbi.1003571>`__

.. [RiboDock2004] Morley, S. D. and Afshar, M. (2004) Validation of an empirical
		  RNA-ligand scoring function for fast flexible docking using
		  RiboDock®. J Comput Aided Mol Des, 18: 189--208.
		  `doi:10.1023/B:JCAM.0000035199.48747.1e <https://doi.org/10.1023/B:JCAM.0000035199.48747.1e>`__
