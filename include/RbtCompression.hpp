#ifndef _RBTCOMPRESSION_H_
#define _RBTCOMPRESSION_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include "Rbt.h"
#ifdef _WIN32
#include <indicators/progress_bar.hpp>
#else
#include <indicators/block_progress_bar.hpp>
#endif

#define DECOMPRESS 24
#define eprintln(err) std::cerr << err << std::endl;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace LE_Bytes {
    RBTDLL_EXPORT uint64_t from(const char* bytes);
    RBTDLL_EXPORT void to(uint64_t uint64, char* destBuf);
}
#endif

namespace CmZ {
    struct RBTDLL_EXPORT archive {
        int mode;
        std::vector<uint64_t> index {};
        std::vector<char> compressedIndex {};
        std::ifstream inputFile;
        std::fstream outputFile;
        std::vector<char> compressedRecord {};
        std::vector<char> decompressedRecord {};
        
        archive() {
            mode = DECOMPRESS;
        };
        archive(int algorithm) {
            mode = algorithm;
        };
        ~archive() {
            if (mode != DECOMPRESS) {
                // Compress index, write it to file, write size to file
                compressIndex();
                uint64_t indexSize = compressedIndex.size();
                outputFile.write(compressedIndex.data(), indexSize);
                char buffer[8];
                LE_Bytes::to(indexSize, buffer);
                outputFile.write(buffer, 8);
            }
            inputFile.close();
            outputFile.close();
        };
        bool readCompressedIndex();
        bool decompressIndex();
        bool compressIndex();
        uint64_t calculateOffset(uint64_t ind);
        bool readRecordAtIndex(uint64_t ind);
        bool readNextRecord();
        bool decompress();
        bool compress();
        bool addCurrentToIndex();
        bool writeRecordToFile();
        uint64_t nextRecord = 0;
    };

    RBTDLL_EXPORT std::string encoderName(unsigned);
    RBTDLL_EXPORT unsigned encoder(unsigned);

    RBTDLL_EXPORT void printSupportedAlgorithms();
    RBTDLL_EXPORT std::vector<uint64_t> rebuildIndex(std::string strInputFile);
#ifdef _WIN32
    RBTDLL_EXPORT std::vector<uint64_t> rebuildIndex(std::string strInputFile,
                    indicators::ProgressBar &progressBar);
#else
    RBTDLL_EXPORT std::vector<uint64_t> rebuildIndex(std::string strInputFile,
                    indicators::BlockProgressBar &progressBar);
#endif
    RBTDLL_EXPORT bool isValidMdlHeader(std::vector<char> input);
    RBTDLL_EXPORT bool isAlgorithmSupported(unsigned iAlgorithm);
    RBTDLL_EXPORT int findAlgorithm(std::vector<char> bytes);
    RBTDLL_EXPORT bool FileIsEmpty(std::string strFileName);
}
#endif //_RBTCOMPRESSION_H_