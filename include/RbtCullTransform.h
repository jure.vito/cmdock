/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * This version is licensed under GNU-LGPL version 3.0 with support from
 * the University of Barcelona.
 * http://rdock.sourceforge.net/
 ***********************************************************************/

// Null transform which does nothing except fire off any requests to the
// scoring function

#ifndef _RBTCULLTRANSFORM_H_
#define _RBTCULLTRANSFORM_H_

#include "RbtBaseTransform.h"

class RbtCullTransform : public RbtBaseTransform {
public:
  // Static data member for class type
  static std::string _CT;
  static std::string _FRAC_LEFT;
  static std::string _CMD;
  static std::string _RECEPTOR_PATH; // TODO: Not the cleanest implementation.

  ////////////////////////////////////////
  // Constructors/destructors
  RbtCullTransform(const std::string &strName = "NULL");
  virtual ~RbtCullTransform();

  ////////////////////////////////////////
  // Public methods
  ////////////////

  virtual void Update(RbtSubject *theChangedSubject); // Does nothing

protected:
  ////////////////////////////////////////
  // Protected methods
  ///////////////////
  virtual void Execute(); // Does nothing

private:
  ////////////////////////////////////////
  // Private methods
  /////////////////
  RbtCullTransform(
      const RbtCullTransform &); // Copy constructor disabled by default
  RbtCullTransform &
  operator=(const RbtCullTransform &); // Copy assignment disabled by default
  
  void RemoveTemporaryFiles(const std::string& posesPath, const std::string& scoresPath);

protected:
  ////////////////////////////////////////
  // Protected data
  ////////////////

private:
  ////////////////////////////////////////
  // Private data
  //////////////
};

// Useful typedefs
typedef SmartPtr<RbtCullTransform> RbtCullTransformPtr; // Smart pointer

#endif //_RBTCULLTRANSFORM_H_
