/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * This version is licensed under GNU-LGPL version 3.0 with support from
 * the University of Barcelona.
 * http://rdock.sourceforge.net/
 ***********************************************************************/

#include "RbtCullTransform.h"
#include "RbtPopulation.h"
#include "RbtWorkSpace.h"
#include "RbtMdlFileSink.h"
#include <filesystem>

// Static data member for class type
std::string RbtCullTransform::_CT("RbtCullTransform");
std::string RbtCullTransform::_FRAC_LEFT("FRAC_LEFT");
std::string RbtCullTransform::_CMD("CMD");
std::string RbtCullTransform::_RECEPTOR_PATH("RECEPTOR_PATH");

////////////////////////////////////////
// Constructors/destructors
RbtCullTransform::RbtCullTransform(const std::string &strName) : RbtBaseTransform(_CT, strName) {
  AddParameter(_FRAC_LEFT, 0.0);
  AddParameter(_CMD, "");
  AddParameter(_RECEPTOR_PATH, "");

  _RBTOBJECTCOUNTER_CONSTR_(_CT);
}

RbtCullTransform::~RbtCullTransform() {
#ifdef _DEBUG
  std::cout << _CT << " destructor" << std::endl;
#endif //_DEBUG
  _RBTOBJECTCOUNTER_DESTR_(_CT);
}

////////////////////////////////////////
// Public methods
////////////////
void RbtCullTransform::Update(RbtSubject *theChangedSubject) {}

// Decodes command by replacing '~' with ' ', since CmDock
// cannot read white spaces in parameters.
std::string decodeCommand(const std::string& encodedCommand) {
  std::string decodedCommand = encodedCommand;
  size_t pos = decodedCommand.find('~');
  while (pos != std::string::npos) {
    decodedCommand.replace(pos, 1, " ");
    pos = decodedCommand.find('~', pos + 1);
  }

  return decodedCommand;
}

////////////////////////////////////////
// Private methods
////////////////
void RbtCullTransform::Execute() {
  RbtWorkSpace* pWorkSpace = GetWorkSpace();
  if (pWorkSpace == nullptr) {
    return;
  }
  RbtPopulationPtr pop = pWorkSpace->GetPopulation();
  if (pop.Null() || (pop->GetMaxSize() < 1)) {
    return;
  }

  std::string workDir = Rbt::GetCurrentWorkingDirectory();
  std::string posesPath = workDir + "/poses.sdf";
  std::string scoresPath = workDir + "/scores.txt";
  std::string receptorPath = GetParameter(_RECEPTOR_PATH);

  // Remove temporary files if they exist.
  this->RemoveTemporaryFiles(posesPath, scoresPath);

  // Save all poses into a file.
  {
    RbtMolecularFileSinkPtr spPosesFileSink(new RbtMdlFileSink(posesPath, RbtModelPtr(), false));
    RbtGenomeList genoms = pop->GetGenomeList();

    for (RbtGenomeListConstIter gIter = genoms.begin(); gIter != genoms.end(); ++gIter) {
      (*gIter)->GetChrom()->SyncToModel();
      GetWorkSpace()->SavePoses(spPosesFileSink);
    }
  }

  // Run given script.
  std::string cmd = decodeCommand(GetParameter(_CMD)) + " " + posesPath + " " + scoresPath + " " + receptorPath;
  std::cout << "Running CMD: " << cmd << std::endl;
  int exitCode = system(cmd.c_str());
  if (exitCode != 0) {
    std::cout << "Command failed with exit code: " << exitCode << "." << std::endl;
    throw RbtError(_WHERE_, "AQDnet exited with an error.");
    return;
  }

  // Read file with scores and parse them.
  std::ifstream file(scoresPath);
  if (!file.is_open()) {
    std::cout << "Error opening the file." << std::endl;
    throw RbtError(_WHERE_, "Failed to open scores path.");
    return;
  }

  std::string line;
  std::getline(file, line);

  file.close();

  std::stringstream ss(line);
  std::vector<double> scores;

  double score;
  while (ss >> score) {
    scores.push_back(score);

    if (ss.peek() == ',') {
      ss.ignore();
    }
  }

  // Remove temporary files if they exist.
  this->RemoveTemporaryFiles(posesPath, scoresPath);

  // Filter vector based on scores.
  double fracLeft = GetParameter(_FRAC_LEFT);
  pop->FilterGenomeList(scores, fracLeft);

  // Save poses after culling.
  std::string culledPosesPath = workDir + "/culled_poses.sdf";
  {
    RbtMolecularFileSinkPtr spPosesFileSink(new RbtMdlFileSink(culledPosesPath, RbtModelPtr(), false));
    RbtGenomeList genoms = pop->GetGenomeList();

    for (RbtGenomeListConstIter gIter = genoms.begin(); gIter != genoms.end(); ++gIter) {
      (*gIter)->GetChrom()->SyncToModel();
      GetWorkSpace()->SavePoses(spPosesFileSink);
    }
  }
}

void RbtCullTransform::RemoveTemporaryFiles(const std::string& posesPath, const std::string& scoresPath) {
  int status = std::remove(posesPath.c_str());
  if (status != 0) {
    std::cout << "Error deleting temporary file with poses." << std::endl;
  }

  status = std::remove(scoresPath.c_str());
  if (status != 0) {
    std::cout << "Error deleting temporary file with scores." << std::endl;
  }
}

